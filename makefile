all: enviaFile recibeFile receive send


recibeFile: recibeFile.cc pct.o
	g++ -pthread -o recibeFile recibeFile.cc pct.o 

enviaFile: enviaFile.cc pct.o
	g++ -pthread -o enviaFile enviaFile.cc pct.o 


send: send.c
	g++ -o send send.c

receive: receive.c
	g++ -o receive receive.c

pct: pct.c pct.h
	g++ -c pct.c

clean:
	rm *.o